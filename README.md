# **JMeter -> InfluxDB -> Grafana - Docker-compose**
## Что это?
Мультиконтейнерный Docker-compose для запуска Jmeter тестов локально, собраный с помощью следующих сервисов:

* [Jmeter 5.4.1](https://gitlab.vkpay.ru/perfomancetests/customjmeter) - кастомовый Jmeter с библиотеками
* [InfluxDB](https://github.com/influxdata/influxdb) - time series DB
* (добавится позже) [Chronograf](https://github.com/influxdata/chronograf) - admin UI for InfluxDB
* [Grafana](https://github.com/grafana/grafana) - онлайн виртуализация метрик из InfluxDB <br>

Во время теста вы сможете просматривать метрики теста в Grafana онлайн. <br>
<img src="https://gitlab.com/flyinkilla/jmeter-influxdb-grafana-in-docker/-/raw/main/img/grafana_1.png" width="70%" height="70%" >

## Предусловия
Убедитесь, что установлены docker, и docker-compose. <br> 
**Achtung!** Данная сборка тестировалась только для MacOS 11.4 и localhost прописан для MacOS(docker.for.mac.localhost).

## Адреса Grafana & InfluxDB

* [Grafana](http://localhost:3000/) :
`http://localhost:3000/`
* [InfluxDB](http://localhost:8086/) :
`http://localhost:8086/`

## Что с этим делать?
* Для начала можно сделать **JMeter docker build image**(тест работоспособности Jmeter) <br> 
`docker build -t jmeter jmeter-docker`

* Или сразу запустить Jmeter, InfluxDB, Grafana из compose. <br> 
`docker-compose up  --build --force-recreate -V -d`

* Запуск теста JMeter <br> 
В терминале `./run-specific-jmeter-script.sh {имя_файла_теста}.jmx` (*.jmx находится локально в каталоге jmeter-scripts)

* Открыть дашборд Grafana [Apache JMeter Dashboard(using Core InfluxdbBackendListenerClient)](http://localhost:3000/dashboards)

* Если что-то пошло не так можно остановить все контейнеры: <br> 
`docker-compose down --remove-orphans -v -t0`

* После проведенного теста вы можете посмотреть логи и отчет локально в каталоге jmeter-scripts/results

## Конфиги теста Jmeter
- В файле **run-specific-jmeter-script.sh** объявлены переменные, и конфигурации запуска.

Объявленные переменные: <br> 

`host` - куда пойдёт нагрузка <br> 
`port` - на какой порт <br> 
`path` - на какой метод(всё, что находится в теле запроса) <br> 


`start_threads_count` - количество тредов(юзеров) в тесте <br> 
`startup_time` - время вхождения всех пользователей(всегда не 0) <br> 
`test_duration_sec` - длительность теста <br> 

Изменяя их, вы меняете параметры теста.

Параметры для запуска теста:
```
-n
This specifies JMeter is to run in cli mode
-t
[name of JMX file that contains the Test Plan].
-l
[name of JTL file to log sample results to].
-j
[name of JMeter run log file].
-r
Run the test in the servers specified by the JMeter property "remote_hosts"
-R
[list of remote servers] Run the test in the specified remote servers
-g
[path to CSV file] generate report dashboard only
-e
generate report dashboard after load test
-o
output folder where to generate the report dashboard after load test. Folder must not exist or be empty
The script also lets you specify the optional firewall/proxy server information:

-H
[proxy server hostname or ip address]
-P
[proxy server port]
```
- **Маунт тестовых файлов и артефактов**:

Файл теста *.jmx и тестовые артефакты(*.csv) находятся локально в каталоге **jmeter-scripts** <br>
Тестовые артефакты примаунчены в Dockerfile(каталог **jmeter-docker**) командой COPY.



## Конфиги Jmeter
В Jmeter переменные теста передаются через параметр терминала -J{anyVarName} <br>
В тесте переменные объявлены как параметр ${__P(anyVarName)}, и этому параметру присваивается переменная. <br>
[Usermanual ${__P}](https://jmeter.apache.org/usermanual/functions.html#__P)

Метрики в InfluxDB передаются по средствам Backend Listener и библиотеки InfluxDBBackendListenerClient. <br>

![](https://jmeter.apache.org/images/screenshots/backend_listener.png) <br>
[Usermanual Jmeter -> InfluxDB](https://jmeter.apache.org/usermanual/realtime-results.html)

## Посмотреть в InfluxDB
В терминале стучитесь в докер: <br>
```
docker ps
docker exec -ti {CONTAINER ID}
influx
show databases
show measurements
jmeter
select * from jmeter where time > now() - 6h
```


## Disclaimer
https://www.blazemeter.com/blog/make-use-of-docker-with-jmeter-learn-how <br>

Thanx to Ilya K & Boris P





