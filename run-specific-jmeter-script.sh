#!/bin/bash

set -e
export timestamp=$(date +%Y%m%d_%H%M%S)
export volume_path=$(pwd)/jmeter-scripts
export jmeter_path=/mnt/jmeter
export host="merchant_box.inplatdev.ru"
export port="80"
export path="api/v1/terminals/"
export start_threads_count="20"
export startup_time="10"
export test_duration_sec="60"

# Reporting dir: start fresh

report_dir=jmeter-scripts/results/report
rm -rf ${report_dir} > /dev/null 2>&1
mkdir -p ${report_dir}

docker run --privileged \
  --volume "${volume_path}":${jmeter_path} \
  jmeter \
  -n \
  -t ${jmeter_path}/$1 \
  -Jinfluxdb_host="docker.for.mac.localhost" \
  -Jhost=${host} -Jport=${port} \
	-Jpath=${path} -Jstart_threads_count=${start_threads_count} \
	-Jstartup_time=${startup_time} -Jtest_duration_sec=${test_duration_sec} \
  -l ${jmeter_path}/results/result_${timestamp}.jtl \
  -j ${jmeter_path}/results/jmeter_${timestamp}.log \
  -e -o ${jmeter_path}/results/report
  
echo "==== jmeter.log ===="
echo jmeter-scripts/results/jmeter_${timestamp}.log

echo "==== HTML Test Report ===="
echo "See HTML test report in jmeter-scripts/results/report/index.html"
